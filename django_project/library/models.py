from django.db import models
import django.utils.timezone


###############                 #################
###############         DB      #################
###############                 #################

class AccountType(models.Model):
    account_type_id = models.AutoField(primary_key=True)
    account_type = models.CharField(max_length=45)
    discount = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'account_type'

class Address(models.Model):
    address_id = models.AutoField(primary_key=True)
    street = models.CharField(max_length=45)
    house_number = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45)
    zip_code = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'address'

class Author(models.Model):
    author_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    surname = models.CharField(max_length=45)
    description = models.ForeignKey('AuthorDescription', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'author'

class AuthorDescription(models.Model):
    description_id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'author_description'

class Book(models.Model):
    book_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=45)
    author = models.ForeignKey(Author, models.DO_NOTHING, db_column='author')
    genre = models.CharField(max_length=45)
    binding = models.ForeignKey('BookBinding', models.DO_NOTHING, db_column='binding')
    description = models.ForeignKey('BookDescription', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        managed = False
        db_table = 'book'

class BookBinding(models.Model):
    binding_id = models.AutoField(primary_key=True)
    binding_type = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'book_binding'

class BookDescription(models.Model):
    description_id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'book_description'

class BookLoan(models.Model):
    book_loan_id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'book_loan'

class Genre(models.Model):
    genre_id = models.AutoField(primary_key=True)
    genre_type = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'genre'

class Readers(models.Model):
    reader_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    surname = models.CharField(max_length=20)
    mail = models.CharField(max_length=45)
    birth_date = models.DateField()
    account_type = models.ForeignKey(AccountType, models.DO_NOTHING)
    favourite_author = models.ForeignKey(Author, models.DO_NOTHING, blank=True, null=True)
    favourite_book = models.ForeignKey(Book, models.DO_NOTHING, blank=True, null=True)
    favourite_genre = models.ForeignKey(Genre, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user'

