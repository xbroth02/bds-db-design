from django.shortcuts import render
from django.views.generic import ListView
from .models import Book

#books = [
#    {
#        'name': 'The Witcher: The Last wish',
#        'author':'Andrzej Sapkowski',
#        'genre':'Fantasy',
#        'binding':'paperback',
#        'description':'The Last Wish (in polish called "Ostatnie życzenie") is the first (in its fictional chronology) of the two collections of short stories preceding the main Witcher Saga, written by Polish fantasy writer Andrzej Sapkowski.',
#    }
#]

def home(request):
    #return HttpResponse('<h1>Library Home</h1>')
    context = {
        'books': Book.objects.all()
    }
    return render(request, 'library/home.html', context)

class BookListView(ListView):
    model = Book
    template_name='library/home.html'    # <app>/<model>_<viewtype>.html
    context_object_name = 'books'

def about(request):
    return render(request, 'library/about.html', {'title':'About'})





#def readers(request):
#    readers=Readers.objects.all()
#    context={
#        'readers':readers
#    }
#    return render(request, 'library/test.html', context)