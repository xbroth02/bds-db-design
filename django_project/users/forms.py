from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from . models import Account

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()      #default:required=True
    name = forms.CharField()
    middleName = forms.CharField(required=False)
    surname = forms.CharField()
    #birth_date = forms.DateField()


    class Meta:
        model = User
        fields = ['username', 'name', 'surname' ,'email','password1','password2']

#class UserUpdateForm(forms.ModelForm):
#    email = forms.EmailField()  # default:required=True
#    birth_date = forms.DateField()

#    class Meta:
#        model = User
#        fields = ['username', 'email']

#class AccountUpdateForm(forms.ModelForm):
#    class Meta:
#        model = Account
#        field = []