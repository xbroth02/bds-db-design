from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
#from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from . forms import UserRegisterForm#, UserUpdateForm, AccountUpdateForm

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            name = form.cleaned_data.get('name')
            surname = form.cleaned_data.get('surname')
            middleName = form.cleaned_data.get('middleName')
            messages.success(request, f'Account for {username} was successfully created, please, log in.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render (request, 'users/register.html', {'form':form})

@login_required()
def account(request):
    #if request.method == 'POST':
    #    u_form = UserUpdateForm(request.POST, instance=request.user)
        #a_form = AccountUpdateForm(request.POST, instance=request.user.profile)
    #    if u_form.is_valid(): #and a_form.is_valid()
    #        u_form.save()
            #a_form.save()
    #        messages.success(request, f'Account has been successfully updated.')
    #        return redirect('profile')
    #else:
    #    u_form=UserUpdateForm(instance=request.user)
        #a_form=AccountUpdateForm(instance=request.user.profile)

     #   context = {
     #       'u_form': u_form,
     #       #'a_form': a_form
     #   }
        return render (request, 'users/account.html')
