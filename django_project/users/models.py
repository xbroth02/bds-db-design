from django.db import models
from django.contrib.auth.models import User

class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE) # if user is deleted, delete account

    def __str__(self):
        return f'{self.user.username} Account'
