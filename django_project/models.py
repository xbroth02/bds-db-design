# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AccountType(models.Model):
    account_type_id = models.AutoField(primary_key=True)
    account_type = models.CharField(max_length=45)
    discount = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'account_type'


class Address(models.Model):
    address_id = models.AutoField(primary_key=True)
    street = models.CharField(max_length=45)
    house_number = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45)
    zip_code = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'address'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Author(models.Model):
    author_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    surname = models.CharField(max_length=45)
    description = models.ForeignKey('AuthorDescription', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'author'


class AuthorDescription(models.Model):
    description_id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'author_description'


class Book(models.Model):
    book_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=45)
    author = models.ForeignKey(Author, models.DO_NOTHING, db_column='author')
    genre = models.CharField(max_length=45)
    binding = models.ForeignKey('BookBinding', models.DO_NOTHING, db_column='binding')
    description = models.ForeignKey('BookDescription', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'book'


class BookBinding(models.Model):
    binding_id = models.AutoField(primary_key=True)
    binding_type = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'book_binding'


class BookDescription(models.Model):
    description_id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'book_description'


class BookHasAuthor(models.Model):
    book = models.ForeignKey(Book, models.DO_NOTHING)
    author = models.ForeignKey(Author, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'book_has_author'


class BookLoan(models.Model):
    book_loan_id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'book_loan'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Genre(models.Model):
    genre_id = models.AutoField(primary_key=True)
    genret_type = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'genre'


class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    user_name = models.CharField(max_length=20)
    surname = models.CharField(max_length=20)
    mail = models.CharField(max_length=45)
    birth_date = models.DateField()
    account_type = models.ForeignKey(AccountType, models.DO_NOTHING)
    favourite_author = models.ForeignKey(Author, models.DO_NOTHING, blank=True, null=True)
    favourite_book = models.ForeignKey(Book, models.DO_NOTHING, blank=True, null=True)
    favourite_genre = models.ForeignKey(Genre, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user'


class UserBorrowsBooks(models.Model):
    book_loan = models.ForeignKey(BookLoan, models.DO_NOTHING)
    user = models.ForeignKey(User, models.DO_NOTHING)
    book = models.ForeignKey(Book, models.DO_NOTHING)
    expiration_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'user_borrows_books'


class UserHasAddress(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    address = models.ForeignKey(Address, models.DO_NOTHING)
    address_type = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'user_has_address'


class UserHasFavouriteAuthor(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    author = models.ForeignKey(Author, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_has_favourite_author'


class UserHasFavouriteBook(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    book = models.ForeignKey(Book, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_has_favourite_book'


class UserHasFavouriteGenre(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    genre = models.ForeignKey(Genre, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_has_favourite_genre'
