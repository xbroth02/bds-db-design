# bds-db-design



## Library

This project creates a database design for a library where there are some books and their authors. There’s also a book loan opportunity for the users which documents the expiration date and the book loan status (in case the book loan is past it’s expiration date or long lost in past).

The users can also add their favourite books, authors or genres which can be in the future used as an information what kind of newsletters the users might want to subscribe to (not part of this particular database design, just a thought about what may be done in the future).

The library wants to have some information about the users - their name, surname, birthdate (which is tied to their account type and their subscription to the library can be for a better price). We also need some contacts for the people, such as their email or address.

Books and authors can also be completed by their short descriptions which can help the users decide what they would like to borrow.

